import random
import json

def rozgrywka():
    cel = random.randint(1,100)
    twoja = 0
    global n
    n=0
    while twoja != cel:
        twoja=int(input("Zgadnij liczbę:\n"))
        if twoja<1 or twoja>100:
            print("Tylko liczby w przedziale 1-100!")
        elif twoja>cel:
            n+=1
            print("Za dużo!")
        elif twoja<cel:
            n+=1
            print("Za mało!")
        else:
            n+=1
            print("Wygrana!\nZajęło Ci to",n,"prób.")
def TabelaWynikow():
    def PokazWyniki():
        sort_tabela=sorted(tabela_s.items(), key=lambda x:x[1])
        sort_tabela=dict(sort_tabela)
        for a,b in sort_tabela.items():
            print(a,b)
    def DodajWynik(nazwa, wynik):
        tabela_s.update({nazwa:wynik})

    plik=open("tabela.json", "r")
    tabela_s=json.load(plik)
    PokazWyniki()
    odp_dodaj=input("Czy chcesz dodać swój wynik do tabeli? [t/n]\n")
    if odp_dodaj=="t":
        imie=input("podaj imię:\n")
        DodajWynik(imie, n)
        json.dump(tabela_s, open("tabela.json", "w"))
        print("Gratulacje!")
        PokazWyniki()
    plik.close()

print("Witaj w grze liczbowej.\nTwoim celem jest zgadnięcie liczby z przedziału 1 do 100.")

odp1="t"
while odp1 == "t":
    rozgrywka()
    odp2=input("Czy chcesz zobaczyć tabelę wyników? [t/n]\n")
    if odp2=="t":
        TabelaWynikow()
    odp1=input("Jeszcze raz? [t/n]\n")
